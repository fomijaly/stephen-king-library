import { Routes } from '@angular/router';
import { ListBooksComponent } from './books/list-books/list-books.component';
import { DetailsBookComponent } from './books/details-book/details-book.component';
import { AuthorComponent } from './shared/author/author.component';


export const routes: Routes = [
    {path: '', component: ListBooksComponent},
    {path: 'book/works/:key', component: DetailsBookComponent},
    {path: 'author', component: AuthorComponent}
];
