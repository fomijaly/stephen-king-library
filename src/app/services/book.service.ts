import { Injectable } from '@angular/core';
import { Book } from '../types/Book';

@Injectable({
  providedIn: 'root',
})
export class BookService {
  constructor() {}

  async getBook() {
    const stockageListe = localStorage.getItem('bookList');
    if (stockageListe) {
      return JSON.parse(stockageListe);
    } else {
      const bookList = await fetch(
        // 'https://openlibrary.org/authors/OL2162284A/works.json'
        'https://openlibrary.org/authors/OL19981A/works.json'
      )
        .then((response) => response.json())
        .then((data) => {
          return data.entries;
        })
        .catch((error) => console.error(error));
      localStorage.setItem('bookList', JSON.stringify(bookList));
      return bookList;
    }
  }

  async getDetailBook(details?: string) {
      const detailBook = await fetch(
        'https://openlibrary.org' + details + '.json'
      )
        .then((response) => response.json())
        .then((data) => {
          return data;
        })
        .catch((error) => console.error(error));
      // console.log('https://openlibrary.org' + details + '.json');
      return detailBook;
  }
  
  async getRateBook(details?: string) {
      const ratingBook = await fetch(
        'https://openlibrary.org' + details + '/ratings.json'
      )
        .then((response) => response.json())
        .then((data) => {
          console.log('rating', data);
          return data;
        })
        .catch((error) => console.error(error));
      // console.log('https://openlibrary.org' + details + '.json');
      return ratingBook;
  }
}
