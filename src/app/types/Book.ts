export type Book = {
  key: string;
  title: string;
  description: {
    type: string;
    value: string;
  };
  summary: {
    average: number;
    count: number;
    sortable: number;
  };
  subtitle: string;
  subjects: [];
  first_publish_date: string;
  _version_: number;
  time_key: Date;
  number_of_pages_median: number;
  contributor: [];
  covers: [];
  isbn: [];
};
