import { Component, Input } from '@angular/core';
import { BookService } from '../../services/book.service';
import { ActivatedRoute, ParamMap } from '@angular/router';
import { Book } from '../../types/Book';

@Component({
  selector: 'app-details-book',
  standalone: true,
  imports: [],
  templateUrl: './details-book.component.html',
  styleUrl: './details-book.component.css',
})
export class DetailsBookComponent {
  bookId?: string;
  book?: Book;

  constructor(public bookService: BookService, private route: ActivatedRoute) {}

  ngOnInit(): void {
    const paramMap: ParamMap | null = this.route.snapshot.paramMap;
    if (paramMap !== null) {
      const bookId = paramMap.get('key');
      if (bookId !== null) {
        this.bookId = '/works/' + bookId;
      }
      this.bookService.getDetailBook(this.bookId).then((data) => {
        this.book = data;
        console.log('resultat getDetail', (this.book = data));
      });

      this.bookService.getRateBook(this.bookId).then((data) => {
        this.book = {...this.book, ...data};
        console.log('resultat getRateBook', (this.book));
      });
    }
  }
}
