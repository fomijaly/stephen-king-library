import { Component } from '@angular/core';
import { BookService } from '../../services/book.service';
import { Book } from '../../types/Book';
import { BookComponent } from '../book/book.component';
import { RouterLink } from '@angular/router';
import { ButtonComponent } from '../../shared/button/button.component';
import { AuthorComponent } from '../../shared/author/author.component';

@Component({
  selector: 'app-list-books',
  standalone: true,
  imports: [RouterLink, BookComponent, ButtonComponent, AuthorComponent],
  templateUrl: './list-books.component.html',
  styleUrls: ['./list-books.component.css', './list-books-responsive.css'],
})
export class ListBooksComponent {
  books?: Book[];
  randomBooks?: Book[];
  bookImages: string[] = [];
  allBooks: [] = [];
  filter = { RegisterBook: '' };

  //Ici que pour l'injection de dépendances
  constructor(public bookService: BookService) {}

  ngOnInit(): void {
    this.bookService.getBook().then((data) => {
      this.allBooks = data;
      this.setData();

      this.randomBooks = this.books
        ?.slice()
        .sort(() => Math.random() - 0.5)
        .slice(0, 1);
      console.log(this.books);
    });
  }

  setData() {
    this.books = [...this.allBooks];
  }

  submitForm(e: Event) {
    e.preventDefault();
    this.filterBooks();
    console.log(this.books)
  }

  filterBooks() {
    if (this.filter.RegisterBook.trim() !== '') {
      this.books = this.allBooks.filter((book: Book) =>
        book.title
          .toLowerCase()
          .includes(this.filter.RegisterBook.toLowerCase())
      );
    } else {
      this.setData();
    }
  }
}
